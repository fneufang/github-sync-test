# gitlab-sync-test

This is a repo that is synced with a GitLab repository.

GitHub Actions syncs this Repo every 15 minutes with https://gitlab.com/fneufang/github-sync-test

A change made on GitLab will be synced to GitHub but not vice versa.

This is a test repository. This concept will be used in the future for multiple of my projects.
